extern crate reqwest; // to download instances lists
extern crate hyper; // headers
#[macro_use] extern crate serde_derive; // deserialitzar json
extern crate crossbeam; // scoped threads.
extern crate domain; // resolve DNS domain and ip
extern crate tokio_core;

use hyper::header::{Headers, Authorization, Bearer};
//use std::thread; // for resolving dns concurrently
//use std::thread::JoinHandle; // for resolving dns concurrently
//use std::time::Duration; // for resolving dns concurrently

// Domain docs https://docs.rs/domain/0.2.3/domain/resolv/index.html
use domain::resolv::Resolver;
use tokio_core::reactor::Core;
use std::net::{IpAddr,Ipv4Addr};
use domain::resolv::lookup::{lookup_host, lookup_addr};
//use  domain::resolv::lookup::host::{FoundHosts,FoundHostsIter};
use domain::bits::DNameBuf;
use std::str::FromStr;

// Sample of reply
/*
   {
   "pagination" : {
      "total" : 766
   },
   "instances" : [
      {
         "obs_score" : 100,
         "active_users" : 814,
         "connections" : 3303,
         "id" : "58e10e8d72782604c488bd35",
         "statuses" : 603661,
         "info" : {
            "short_description" : "Generalistic and moderated instance. All opinions are welcome, but hate speeches are prohibited.",
            "languages" : [
               "en",
               "fr"
            ],
            "other_languages_accepted" : true,
            "prohibited_content" : [
               "nudity_nocw",
               "pornography_nocw",
               "illegalContentLinks",
               "spam"
            ],
            "full_description" : "Generalistic and moderated instance. All opinions are welcome, but hate speeches are prohibited.\r\n\r\nUsers who don't respect rules will be silenced, or suspended if they harass other people or if the the violation is considered too large.",
         },
         "name" : "mastodon.xyz",
         "open_registrations" : true,
         "users" : 15605,
      },
*/

#[derive(Deserialize)]
struct JsonPagination {
	total: u64,
}

#[derive(Deserialize)]
struct JsonInfo {
    short_description: String,
    full_description: String,
    languages: Vec<String>,
    prohibited_content: Vec<String>,
    other_languages_accepted: bool,
}

#[derive(Deserialize)]
struct JsonInstance {
    info: Option<JsonInfo>,
    //users: u64,
    users: String,
    active_users: u64,
    //connections: u64,
    connections: String,
    //statuses: u64,
    statuses: String,
    name: String,
}

#[derive(Deserialize)]
struct JsonInstancesSocial {
	pagination: JsonPagination,
    instances: Vec<JsonInstance>,
}

struct NodeStats {
    //users: u64,
    users: String,
    users_active: u64,
    //entries: u64,
    entries: String,
}

struct Node {
    domain: String,
    //a: Option<String>,
    a: Option<Ipv4Addr>,
    ptr: Option<String>,
    ptr_name: Option<String>,
    stats: NodeStats,
}

fn resolve_names(n: &mut Node) -> (){
    /*
    n.a = Some("fake ip".to_string());
    n.ptr = Some("fake ptr".to_string());
    n.ptr_name = Some("fake ptr short".to_string());
    */
    // Prepare the network runtime
    let mut core = Core::new().unwrap();
    let resolv = Resolver::new(&core.handle());

    // Resolve host name
    let name = DNameBuf::from_str(&n.domain).unwrap();
    let query = lookup_host(resolv.clone(), name);
    let result = core.run(query).unwrap();
    n.a = match result.iter().next() {
       Some(IpAddr::V4(a)) => Some(a),
        _ => None
    };
    // Resolve ip hostname
    let query = lookup_addr(resolv, IpAddr::V4(n.a.unwrap()));
    let result = match core.run(query) {
        Err(e) => n.ptr = Some("not found".to_string()),
        res @ _=> n.ptr = Some(format!("{:?}",res.iter().next().unwrap().iter().next())),
    };

    // Shorten reverse hostname
}

fn main() {
    const LIST_MAX_NODES: u64 = 100;
    const MIN_ACTIVE_USERS: u64 = 1;
    let list_url: String = format!("https://instances.social/api/1.0/instances/\
                           list?count={}&include_down=false&include_closed=false\
                           &min_version=2.1.2&min_active_users={}",
                           LIST_MAX_NODES,
                           MIN_ACTIVE_USERS
    );

    let http_client = reqwest::Client::new();

    let mut headers = Headers::new();
    headers.set(Authorization(Bearer {
        token: "vyIS19U6VB4YBzeSwxur4IKPnrhmX0kCAemSFDdrkPXC9ivCBYZWkyVNXqzZp22ev3zmVYOJhuJmIjHTOgalMF1LGBKI4EFQn2fI3cJ8e9v1ThCxTwFha7dbHoxDDNC6".to_owned()
    }));
    let resp = http_client.get(&list_url)
        .headers(headers)
        .send();

    // Deserialitzar json a un struct propi
    let j: JsonInstancesSocial = resp.expect("Could not download nodes list").json().unwrap();

    let mut nodes: Vec<Node>;

    nodes = j.instances.iter().map(|j_node| {
        Node {
            domain: j_node.name.to_owned(),
            a: None,
            ptr: None,
            ptr_name: None,
            stats: NodeStats {
                users: j_node.users.to_owned(),
                users_active: j_node.active_users,
                entries: j_node.statuses.to_owned(),
            }
        }
    }).collect();

    for mut node in nodes {
        crossbeam::scope(|scope| {
            scope.spawn(|| {
                resolve_names(&mut node);
                println!("Node {} | active users {}/{} | ip {:?} | reverse domain {:?}",
                         node.domain, node.stats.users_active, node.stats.users, node.a, node.ptr);
            });
        });
    }

}
