## Procediment
1. Entrada. Determinar quina llista descarregar
2. Descarregar la/les llistes d'instàncies
3. Extreure'n els dominis, els noms, nombre d'usuàries, publicacions, usuàries actives...
4. Resoldre'n la IP, el domini invers de la IP, i el "domini invers curt".
5. Desa en taula csv
6. Desa'n un resum (count)
7. Grafica amb el gnuplot

## Estructura
* Estructura genèrica de llista de servidors.
* Implementacions per a the-fediverse.info, instances.social.
* Gestió de les opcions d'entrada
* Tipus "script" en passos per funcions.
* Execució:
  1. Encadena tres funcions futures
    1. resoldre el domini (nom → ip)
    2. resoldre el dominin invers, escurçar el domini invers
    2. resoldre la ASN de BGP per a la IP i consulta'n la propietària
  2. Crea un vector amb totes aquestes cadenes de funcions.
  3. Llença un nombre limitat de threads que vagin executant aquestes funcions a mesura que vagin acabant.

## Mòduls requerits
* Descàrrega HTTP → hyper (no, és asíncrona, massa complicat) → reqwest
* Domini invers → domain: al dns regular, al dns de team cymru, al whois?
* Json → serde_json
* Gràfics (diversos motors, en faig un de simple per defecte)

## Whois a una IP
A diferència dels dominis inversos (DNS), les IP estan sempre registrades (whois).
Però en canvi, la informació no està tan ordenada.
Recopilem exemples.
Les claus que més semblen representar el que volem trobar (de qui és la IP), semblen ser:
* org-name
* OrgName

I en segon lloc:
* org
* organisation
* Organization

Amb noms menys clars i més variables però útils
* NetName
* netname

I altra informació útil:
* country/Country
* origin/OriginAS

De vegades és molt rellevant el camp `descr`, però de vegades gens, així que no el podem usar.
Una altra observació: la informació més rellevant sol trobar-se al final:
la més actualitzada o la més precisa, quan hi ha diverses seccions.

Per tal de fer-la servir, cal marcar quin ha estat l'últim camp tipus "orgname" que s'ha trobat.

cyberciti.biz
```
$ whois 104.20.187.5
NetRange:       104.16.0.0 - 104.31.255.255
CIDR:           104.16.0.0/12
NetName:        CLOUDFLARENET
OriginAS:       AS13335
Organization:   Cloudflare, Inc. (CLOUD14)

OrgName:        Cloudflare, Inc.
OrgId:          CLOUD14
Country:        US
OrgAbuseName:   Abuse
OrgTechName:   Admin
```

masto.host
```
$ host 51.255.95.5
5.95.255.51.in-addr.arpa domain name pointer host.umanuvem.com.

$ whois 51.255.95.5
% Information related to '51.254.0.0 - 51.255.255.255'

netname:        FR-OVH-20150522
descr:          OVH SAS
country:        FR
organisation:   ORG-OS3-RIPE
org-name:       OVH SAS

% Information related to '51.254.0.0/15AS16276'

route:          51.254.0.0/15
descr:          OVH
origin:         AS16276
```

social.coop
```
$ host 51.15.35.196
196.35.15.51.in-addr.arpa domain name pointer 196-35-15-51.rev.cloud.scaleway.com.


$ whois 51.15.35.196
% Information related to '51.15.0.0 - 51.15.63.255'

org:            ORG-ONLI2-RIPE
netname:        ONLINE_NET_DEDICATED_SERVERS_NL
country:        NL

organisation:   ORG-ONLI2-RIPE
org-name:       ONLINE SAS NL

% Information related to '51.15.0.0/16AS12876'

origin:         AS12876
```

quitter.es
```
$ host 85.11.25.68
68.25.11.85.in-addr.arpa is an alias for 85-11-25-68.hethane.riksnet.nu.
85-11-25-68.hethane.riksnet.nu domain name pointer ip68.hethane.riksnet.nu.

$ whois 85.11.25.68
% Information related to '85.11.25.64 - 85.11.25.79'
netname:        SE-RIKSNET-UMETV
descr:          UMETV
country:        SE
org:            ORG-RIKi1-RIPE

organisation:   ORG-RIKI1-RIPE
org-name:       Ratt Internet Kapacitet i Sverige AB

% Information related to '85.11.0.0/18AS34610'

descr:          Ratt Internet Kapacitet i Sverige AB
descr:          Riksnet
origin:         AS34610
```

mastodon.xyz
```
$ host 163.172.251.107
107.251.172.163.in-addr.arpa domain name pointer laseri.thekinrar.fr.


$ whois 163.172.251.107
% Information related to '163.172.0.0 - 163.172.255.255'

org:            ORG-ONLI1-RIPE
netname:        ONLINE_NET_DEDICATED_SERVERS
descr:          Dedicated Servers and cloud assignment, abuse reports : http://abuse.online.net
country:        FR

organisation:   ORG-ONLI1-RIPE
mnt-ref:        MNT-TISCALIFR-B2B
org-name:       ONLINE SAS

% Information related to '163.172.0.0/16AS12876'

descr:          Online SAS
descr:          Paris, France
origin:         AS12876
```

pawoo.net
```
# start

NetRange:       13.112.0.0 - 13.115.255.255
CIDR:           13.112.0.0/14
NetName:        AT-88-Z
Organization:   Amazon Technologies Inc. (AT-88-Z)

OrgName:        Amazon Technologies Inc.
OrgId:          AT-88-Z
Country:        US

OrgTechName:   Amazon EC2 Network Operations
OrgNOCName:   Amazon AWS Network Operations

# end


# start

NetRange:       13.112.0.0 - 13.115.255.255
CIDR:           13.112.0.0/14
NetName:        AMAZON-NRT
OriginAS:       AS16509
Organization:   Amazon Data Services Japan (AMAZO-49)

OrgName:        Amazon Data Services Japan
OrgId:          AMAZO-49
Country:        JP

OrgAbuseName:   Amazon EC2 Abuse
OrgTechName:   Amazon EC2 Network Operations
OrgNOCName:   Amazon AWS Network Operations

# end


# start

NetRange:       13.112.0.0 - 13.115.255.255
CIDR:           13.112.0.0/14
NetName:        AMAZON-NRT
OriginAS:       AS16509
Organization:   Amazon Data Services Japan (AMAZO-49)

OrgName:        Amazon Data Services Japan
OrgId:          AMAZO-49
Country:        JP
OrgNOCName:   Amazon AWS Network Operations

OrgAbuseName:   Amazon EC2 Abuse
OrgTechName:   Amazon EC2 Network Operations

# end
```

mstdn.jp
```
% Information related to '27.133.128.0 - 27.133.159.255'

netname:        SAKURA
country:        JP
country:        JP

% Information related to '27.133.147.0 - 27.133.147.255'

netname:        SAKURA-NET
country:        JP
```

mastodon.social
```
NetRange:       136.243.0.0 - 136.243.255.255
NetName:        RIPE-ERX-136-243-0-0
OriginAS:
Organization:   RIPE Network Coordination Centre (RIPE)

OrgName:        RIPE Network Coordination Centre
Country:        NL

OrgTechName:   RIPE NCC Operations
OrgAbuseName:   Abuse Contact

% Information related to '136.243.102.128 - 136.243.102.191'

netnum:        136.243.102.128 - 136.243.102.191
netname:        HETZNER-fsn1-dc8
country:        DE

org:            ORG-HOA1-RIPE

% Information related to '136.243.0.0/16AS24940'

origin:         AS24940
org:            ORG-HOA1-RIPE

organisation:   ORG-HOA1-RIPE
org-name:       Hetzner Online GmbH
```
